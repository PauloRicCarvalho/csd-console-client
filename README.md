# Console Client

The game loop is already implemented, and in order to make it work you just need
to connect your libraries here.

You can add some color to your client using the following packages:

* 📦 [Colors](https://www.npmjs.com/package/colors)
* 📦 [Chalk](https://www.npmjs.com/package/chalk)
* 📦 [Terminal-Kit](https://www.npmjs.com/package/terminal-kit)
* 📦 [Cli-Artist](https://www.npmjs.com/package/cli-artist)
* 📦 [Text-Draw](https://www.npmjs.com/package/text-draw)
* 📦 [Line-JS](https://www.npmjs.com/package/lines-js)

The npm registry is also full of other packages you can use to increment your
console client. Check it out:
[NPM Registry](https://www.npmjs.com/search?q=console)
